﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Collectable", menuName ="Collectable")]
public class collectable2 : ScriptableObject {

    public Sprite ArtWork;
    public string Description;

    public string type;//bad or good
    public int points;
    public float speed; 
}
