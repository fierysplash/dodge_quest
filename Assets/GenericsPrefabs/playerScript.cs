﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playerScript : MonoBehaviour
{
    public Vector3[] lanePoss;
    public RectTransform rect;
    private bool moving;
    private Vector3 targetPos;
    public difficulty difficulty;
    private bool boosting;
    public GameObject particles;

    void Update()
    {
        if (!difficulty.Playing)
        {
            return;
        }

        if (moving)  // if moving  keep moving char to designated mos. 
        {
            moveChar();
        }
        else       // else check for incoming movement requests
        {
            if (!Application.isMobilePlatform)
                checkMovement();
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {

        if (collision.tag == "collectable")
        {
            if (collision.gameObject.GetComponent<collectable>().type == "coin")
            {
                collision.gameObject.SetActive(false);
                difficulty.addCoin();
            }

            if (collision.gameObject.GetComponent<collectable>().type == "ball")
            {
                collision.gameObject.SetActive(false);
                difficulty.removeCoin();
            }

            if (collision.gameObject.GetComponent<collectable>().type == "bomb")
            {
                collision.gameObject.SetActive(false);
                difficulty.removeLife();
            }

        }
        else if (collision.tag == "answer")
        {
            difficulty.boost = 1;
            if (boosting)
            {
                difficulty.addDifficulty();
                difficulty.addCoin();
                difficulty.addCoin();
                difficulty.addCoin();
                boosting = false;
                particles.SetActive(false);
            }
            else
            {
                difficulty.addCoin();
                difficulty.addCoin();
            }


            difficulty.SpawnGatesDeactivate();
            difficulty.addDifficulty();
            StartCoroutine(difficulty.SpawningCoinGamePlay());
        }
        else if (collision.tag == "question") // question section
        {
            difficulty.boost = 1;
            if (boosting)
            {
                particles.SetActive(false);
                boosting = false;
                difficulty.removeLife();
                
            }
            else
            {
                difficulty.removeCoin();
                difficulty.removeCoin();

            }

            difficulty.SpawnGatesDeactivate();
            difficulty.reduceDifficulty();
            StartCoroutine(difficulty.SpawningCoinGamePlay());
        }
        else
        {

            Debug.Log("Not Scripted collectable");
        }
    }

    private void checkMovement()

    {
        if (Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.LeftArrow))
        {
            MoveLeft();     //checks lane and moves player left
        }
        if (Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.RightArrow))
        {
            MoveRight();    //checks lane and moves player right
        }
        if (Input.GetKey(KeyCode.UpArrow) && difficulty.canBoost == true)
        {
            boost();
        }
    }

    private void moveChar()
    {
        rect.position = Vector3.MoveTowards(new Vector3(rect.position.x, rect.position.y, rect.position.z), targetPos, (difficulty.MS * Time.deltaTime) * 1.1f);

        if (rect.position == targetPos)
        {
            moving = false;
        }
    }

    public void NavigateAndBoost(string lane)
    {
        if (boosting)
        {
            return;
        }
        int ilane = int.Parse(lane);
        targetPos = lanePoss[ilane];
        moving = true;
        boost();
    }

    public void MoveLeft()
    {
        if (moving || boosting)
            return;

        if (rect.position == lanePoss[0])
            return;


        if (rect.position == lanePoss[1])
        {
            targetPos = lanePoss[0];
        }

        if (rect.position == lanePoss[2])
        {
            targetPos = lanePoss[1];
        }
        moving = true;
    }

    public void MoveRight()
    {
        if (moving || boosting)
            return;


        if (rect.position == lanePoss[2])
            return;


        if (rect.position == lanePoss[1])
        {
            targetPos = lanePoss[2];
        }

        if (rect.position == lanePoss[0])
        {
            targetPos = lanePoss[1];
        }
        moving = true;
    }

    public void boost()
    {
        if (!difficulty.canBoost)
        {
            return;
        }
        boosting = true;
        difficulty.boost = 3;
        particles.SetActive(true);
    }
}
