﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class difficulty : MonoBehaviour
{
    Player player;
    public float MS;
    public List<GameObject> ActiveCoins;//object pooling
    public List<GameObject> ActiveBalls;
    public List<GameObject> ActiveBombs;
    public List<GameObject> QGate;
    public GameObject questionTextField;
    public TextMesh Q1, Q2, Q3;
    public Text hint1, hint2, hint3;
    public int score = 0;
    public int pooledAmount = 24;
    public GameObject coin, ball, bomb;//prefabs
    public Vector3[] lanePoss;  //positions for things to spawn from
    public bool Playing = true;
    public Text SPM, scoreTxt,  //in game text fields
        EndScore, EndSpeed, EndMaxSpeed, EndMaxScore;//game over text fields
    public Image[] lives;
    public float highScoreSpeed;
    public float highScore;
    public int life = 3;
    public GameObject GameOverMenu;
    public Sprite cloud2;
    private int questionType=1;//addition is 1
    public float questionScale=10;
    public int boost=1;
    public bool canBoost = false;
    private void OnEnable()//Begin Object pooling
    {
        for (int i = 0; i < pooledAmount; i++)
        {
            GameObject obj = Instantiate(coin);
            obj.SetActive(false);
            ActiveCoins.Add(obj);
            GameObject obj2 = Instantiate(ball);
            obj2.SetActive(false);
            ActiveBalls.Add(obj2);
            GameObject obj3 = Instantiate(bomb);
            obj3.SetActive(false);
            ActiveBombs.Add(obj3);
        }
    }
    void Start()
    {
        player = FindObjectOfType<Player>();
        startQuestionDifficulty();

        highScore = player.highScore;
        highScoreSpeed = player.topSpeed;

        StartCoroutine(SpawningCoinGamePlay());//run spawning as a coroutine
        StartCoroutine(MoveObjects());
    }

    private void startQuestionDifficulty()
    {
        if (player.difficulty == 1)
        {
            questionScale = player.age * 1.125f;
        }
        if (player.difficulty == 2)
        {
            questionScale = player.age * 1.25f;
        }
    }

    public void addDifficulty()
    {
        questionScale += player.age / 5;
    }

    public void reduceDifficulty()
    {
        questionScale = questionScale * 0.9f;
    }

    private void MoveObjectsDown(List<GameObject> Gobjects)
    {
        foreach (GameObject c in Gobjects)
        {
            if (!c.activeInHierarchy)
            {
                continue;
            }

            c.transform.position = new Vector3(c.transform.position.x, c.transform.position.y - MS * Time.deltaTime*boost, c.transform.position.z);
            if (c.transform.position.y < -5)//below screen.
            {
                c.SetActive(false);
            }
        }
    }



    private void disableGos(List<GameObject> Gobjects)
    {
        
        foreach (GameObject c in Gobjects)
        {
            if (!c.activeInHierarchy)
            {
                continue;
            }

            c.SetActive(false);
        }
    }

    public IEnumerator SpawningCoinGamePlay()
    {
        canBoost = false;
        float counterPlayTime = 0;
        while (counterPlayTime < 10 && Playing)
        {
            counterPlayTime += SpawnTime();
            print("Counter"+counterPlayTime);
            SpawnCoins();
            setText();
            if (counterPlayTime > 10)
            {
                SpawnChoiceGamePlay();
            
            }
            yield return new WaitForSeconds(SpawnTime());
        }

    }

    private void  SpawnChoiceGamePlay()//called after 10 seconds of 
    {
        askQuestion();
        SpawnGates(); //set all objects to inactive
        canBoost = true;
    }

    private void SpawnGates()
    {
        disableGos(ActiveBalls);
        disableGos(ActiveBombs);
        disableGos(ActiveCoins);
        QGate[0].transform.position = new Vector3(QGate[0].transform.position.x, MS * 5,2);//5 seconds to figure answer out
        QGate[0].SetActive(true);
    }

    public void SpawnGatesDeactivate()//called from player
    {
        questionTextField.SetActive(false);
        QGate[0].SetActive(false);
        hint1.gameObject.SetActive(false);
        hint2.gameObject.SetActive(false);
        hint3.gameObject.SetActive(false);
    }

    private void askQuestion()
    {
        int tempScale = (int)questionScale + UnityEngine.Random.Range(-1, 1);
        hint1.gameObject.SetActive(true);
        hint2.gameObject.SetActive(true);
        hint3.gameObject.SetActive(true);

        switch (questionType)
        {
            case 1://addition
                int valueOne = (int) UnityEngine.Random.Range(tempScale * 0.2f, tempScale * 0.8f); // random value for the first value in the question line
                int valueTwo = (int)tempScale - valueOne; // second value in the question line
                questionTextField.SetActive(true);
                questionTextField.GetComponent<Text>().text = valueOne + " + " + valueTwo + " ="; // the question line

                int answer = valueOne+valueTwo;
                int answerWrong1 = (int)UnityEngine.Random.Range(tempScale * 0.80f, tempScale * 0.99f);
                if (answerWrong1 == answer)
                {
                    answerWrong1 -= 1;
                }
                int answerWrong2 = (int)UnityEngine.Random.Range(tempScale * 1.01f, tempScale * 1.20f);
                if (answerWrong2 == answer)
                {
                    answerWrong2 += 1;
                }

                switch(UnityEngine.Random.Range(1, 3))
                {
                    case 1:
                        hint1.text = answer.ToString();
                        Q1.text = answer.ToString();
                        Q1.gameObject.tag = "answer";
                        if (UnityEngine.Random.Range(1, 2) == 1)
                        {
                            hint2.text = answerWrong1.ToString();
                            Q2.text = answerWrong1.ToString();
                            Q2.gameObject.tag = "question";
                            hint3.text = answerWrong2.ToString();
                            Q3.text = answerWrong2.ToString();
                            Q3.gameObject.tag = "question";
                        }
                        else
                        {
                            hint3.text = answerWrong1.ToString();
                            Q3.text = answerWrong1.ToString();
                            Q3.gameObject.tag = "question";
                            hint2.text = answerWrong2.ToString();
                            Q2.text = answerWrong2.ToString();
                            Q2.gameObject.tag = "question";
                        }
                        break;

                    case 2:
                        hint2.text = answer.ToString();
                        Q2.text = answer.ToString();
                        Q2.gameObject.tag = "answer";
                        if (UnityEngine.Random.Range(1, 2) == 1)
                        {
                            hint1.text = answerWrong1.ToString();
                            Q1.text = answerWrong1.ToString();
                            Q1.gameObject.tag = "question";
                            hint3.text = answerWrong2.ToString();
                            Q3.text = answerWrong2.ToString();
                            Q3.gameObject.tag = "question";
                        }
                        else
                        {
                            hint3.text = answerWrong1.ToString();
                            Q3.text = answerWrong1.ToString();
                            Q3.gameObject.tag = "question";
                            hint1.text = answerWrong2.ToString();
                            Q1.text = answerWrong2.ToString();
                            Q1.gameObject.tag = "question";
                        }
                        break;

                    case 3:
                        hint3.text = answer.ToString();
                        Q3.text = answer.ToString();
                        Q3.gameObject.tag = "answer";
                        if (UnityEngine.Random.Range(1, 2) == 1)
                        {
                            hint1.text = answerWrong1.ToString();
                            Q1.text = answerWrong1.ToString();
                            Q1.gameObject.tag = "question";
                            hint2.text = answerWrong2.ToString();
                            Q2.text = answerWrong2.ToString();
                            Q2.gameObject.tag = "question";
                        }
                        else
                        {
                            hint2.text = answerWrong1.ToString();
                            Q2.text = answerWrong1.ToString();
                            Q2.gameObject.tag = "question";
                            hint1.text = answerWrong2.ToString();
                            Q1.text = answerWrong2.ToString();
                            Q1.gameObject.tag = "question";
                        }
                        break;
                }               
                break;
        }

    }

    IEnumerator MoveObjects()//this causes the downward movement of all objects in the game 
    {

        while (Playing)
        {
            MoveObjectsDown(QGate);
            MoveObjectsDown(ActiveBalls);
            MoveObjectsDown(ActiveBombs);
            MoveObjectsDown(ActiveCoins);
            yield return new WaitForEndOfFrame();
        }

    }

    private float SpawnTime()//how frequently you encounter objects
    {
        return 6 / MS;
    }

    public void ResetGame()
    {
        startQuestionDifficulty();
        Playing = true;
        MS = 5;
        score = 0;
        lives[0].gameObject.SetActive(true);
        lives[1].gameObject.SetActive(true);
        lives[2].gameObject.SetActive(true);
        life = 3;
        disableGos(ActiveBalls);
        disableGos(ActiveBombs);
        disableGos(ActiveCoins);
        QGate[0].SetActive(false);
        StartCoroutine(SpawningCoinGamePlay());//run spawning as a coroutine
        StartCoroutine(MoveObjects());
    }

    private void SpawnCoins()
    {
        if (MS > 9)
        {
            SpawnHard();
        }
        else if (MS > 6)
        {
            SpawnMed();
        }
        else
        {
            SpawnEasy();
        }
    }

    private void SpawnMed()//2safe
    {
        //spawn only positive choice

        if (UnityEngine.Random.Range(0, 3) != 0)
        {
            SpawnCoinAtPos(lanePoss[UnityEngine.Random.Range(0, 3)]);
        }
        else
        {
            SpawnBallAtPos(lanePoss[UnityEngine.Random.Range(0, 3)]);
        }

    }

    private void SpawnEasy()//2safe
    {
        if (UnityEngine.Random.Range(0, 3) == 0)
        {
            SpawnCoinAtPos(lanePoss[UnityEngine.Random.Range(0, 3)]);
        }
        else
        {
            int check = UnityEngine.Random.Range(0, 3);
            if (check == 2)
            {
                SpawnCoinAtPos(lanePoss[1]);
                SpawnCoinAtPos(lanePoss[2]);
                return;
            }
            else if (check == 1)
            {
                SpawnCoinAtPos(lanePoss[0]);
                SpawnCoinAtPos(lanePoss[2]);
            }
            else
            {
                SpawnCoinAtPos(lanePoss[1]);
                SpawnCoinAtPos(lanePoss[0]);
            }

        }
    }

    private void SpawnHard()//1safe
    {
        int check = UnityEngine.Random.Range(0, 3);
        if (check == 0)
        {
            SpawnCoinAtPos(lanePoss[UnityEngine.Random.Range(0, 3)]);
        }
        else if (check == 1)
        {
            SpawnBallAtPos(lanePoss[UnityEngine.Random.Range(0, 3)]);
        }
        else
        {
            SpawnBombAtPos(lanePoss[UnityEngine.Random.Range(0, 3)]);
        }


    }

    private void SpawnCoinAtPos(Vector3 pos)//called from coin spawner, use from pool
    {
        foreach (GameObject c in ActiveCoins)
        {
            //print(c.activeInHierarchy + " vs " + c.activeSelf);
            if (c.activeInHierarchy)
            {
                continue;
            }

            c.SetActive(true);
            c.transform.position = pos;
            c.transform.rotation = Quaternion.Euler(new Vector3(0, 0, UnityEngine.Random.Range(0f, 360f)));
            break;
        }

    }

    private void SpawnBallAtPos(Vector3 pos)//called from spawner, use from pool
    {
        foreach (GameObject c in ActiveBalls)
        {
            //print(c.activeInHierarchy + " vs " + c.activeSelf);
            if (c.activeInHierarchy)
            {
                continue;
            }

            c.SetActive(true);
            c.transform.position = pos;
            c.transform.rotation = Quaternion.Euler(new Vector3(0, 0, UnityEngine.Random.Range(0f, 360f)));
            break;
        }

    }

    private void SpawnBombAtPos(Vector3 pos)//called from spawner, use from pool
    {
        foreach (GameObject c in ActiveBombs)
        {
            //print(c.activeInHierarchy + " vs " + c.activeSelf);
            if (c.activeInHierarchy)    //looks for non-active GO to use
            {
                continue;
            }

            c.SetActive(true);
            c.transform.position = pos; //spawns at target pos
            c.transform.rotation = Quaternion.Euler(new Vector3(0, 0, UnityEngine.Random.Range(0f, 360f)));
            break;
        }

    }

    private void setText()
    {
        if (MS > highScoreSpeed)
        {
            highScoreSpeed = MS;
        }
        SPM.text = System.Math.Round(MS, 2).ToString() + "m/s";
        scoreTxt.text = score.ToString();
    }

    public void addCoin()
    {
        score += 100;
        MS *= 1.01f;

        //print("Got a coin");
        //Debug.Log("Got a coin DB");
    }

    public void removeCoin()
    {
        score -= 150;
        MS /= 1.015f;
    }

    public void removeLife()
    {
        if (life > 0)
        {
            score -= 200;
            MS /= 1.03f;
            lives[life - 1].gameObject.SetActive(false);
            life--;
            //print("Lost A Life");//Not working yet in alpha
        }
        else
        {
            if (score > highScore)
            {
                highScore = score;
            }
            Playing = false;
            GameOverMenu.SetActive(true);
            EndMaxScore.text = highScore.ToString();
            EndMaxSpeed.text = System.Math.Round(highScoreSpeed, 2).ToString() + "m/s";//
            EndScore.text = score.ToString();
            EndSpeed.text = System.Math.Round(MS, 2).ToString() + "m/s";

            player.highScore = (int)highScore;
            player.topSpeed = highScoreSpeed;
            SaveSystem.SavePlayer(player);
            //Debug.Log("Game Over");
            //print("Game Over");

        }
    }

}
