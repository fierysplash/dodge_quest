﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwipeDetector : MonoBehaviour
{
    public playerScript ps;

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape)) { Application.Quit(); }

        foreach (Touch touch in Input.touches)
        {


            if (touch.position.x < Screen.width / 2 && touch.position.y < Screen.height / 2)
            {
                print("Left click" + touch.position + "  xxxx  " + touch.rawPosition);
                TouchLeft();
            }
            else if (touch.position.x > Screen.width / 2 && touch.position.y < Screen.height / 2)
            {
                print("Right click" + touch.position + "  xxxx  " + touch.rawPosition);
                TouchRight();
            }else if(touch.position.y > Screen.height / 2)
            {
                //touchUP();
            }



        }
    }


    //////////////////////////////////CALLBACK FUNCTIONS/////////////////////////////
    void touchUP()
    {
        ps.boost();


    }

    void TouchLeft()
    {
        ps.MoveLeft();
  
    }

    void TouchRight()
    {
        ps.MoveRight();


    }
}
