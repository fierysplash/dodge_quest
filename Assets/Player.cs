﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Player : MonoBehaviour {

    public int age = 0;
    public float topSpeed = 0;
    public int highScore = 0;
    public int difficulty = 0; //0 easy 1 medium 2 hard

    private static Player _instance;

    public static Player Instance { get { return _instance; } }


    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }
    }

    public void StartScript()

    {
        DontDestroyOnLoad(this);
        LoadPlayer();
    }

    public void SavePlayer()
    {
        SaveSystem.SavePlayer(this);
    }

    public void LoadPlayer()
    {
        
        PlayerData data = SaveSystem.LoadPlayer();
        if (data == null)
            return;
        age = data.age;
        topSpeed = data.topSpeed;
        highScore = data.highScore;
    }
}
