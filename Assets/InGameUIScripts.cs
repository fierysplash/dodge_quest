﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class InGameUIScripts : MonoBehaviour {

    public void NavigateToMenu()
    {
        SceneManager.LoadScene("MenuScene");
    }
}
