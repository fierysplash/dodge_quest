﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
public class MenuSystem : MonoBehaviour {

    public Player player;
    public GameObject playNext1;
    public GameObject playNext2;
    public InputField ageInput;

    public void Start()
    {
        player = FindObjectOfType<Player>();
        player.StartScript();
    }
    public void PlayNext1()
    {
        if(player.age == 0)
        {
            playNext1.SetActive(true);
           

        }
        else
        {
            playNext2.SetActive(true);
        }
    }

    public void ageChanged()
    {
        int ageInt = int.Parse(ageInput.text);
        if (ageInt > 0)
        {
            playNext2.SetActive(true);
            player.age = ageInt;
            SaveSystem.SavePlayer(player);
        }
    }

    public void Easy()
    {
        player.difficulty = 0;

        SceneManager.LoadScene("Play"); 
    }

    public void Medium()
    {
        player.difficulty = 1;

        SceneManager.LoadScene("Play");
    }

    public void Hard()
    {
        player.difficulty = 3;

        SceneManager.LoadScene("Play");
    }
}
