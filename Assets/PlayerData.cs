﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class PlayerData {

    public int age;
    public float topSpeed;
    public int highScore;

    public PlayerData (Player player)
    {
        age = player.age;
        topSpeed = player.topSpeed;
        highScore = player.highScore;
    }

}
